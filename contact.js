document.getElementById('contact-form').addEventListener('submit', function(e) {
    e.preventDefault();

    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const message = document.getElementById('message').value;

    const webhookURL = 'https://discord.com/api/webhooks/1248630621476225035/DZnzbZJOIBW0VIPQRHKme5YSuLoj4C7sbEpDSle5-0gf9g0xYvFbMne_ofC9YJX5D283';

    const payload = {
        content: `Nowa wiadomość z formularza kontaktowego:\n\n**Imię:** ${name}\n**Email:** ${email}\n**Wiadomość:** ${message}`
    };

    fetch(webhookURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    })
    .then(response => {
        if (response.ok) {
            alert('Wiadomość została wysłana!');
        } else {
            alert('Wystąpił błąd. Spróbuj ponownie później.');
        }
    })
    .catch(error => {
        console.error('Error:', error);
        alert('Wystąpił błąd. Spróbuj ponownie później.');
    });

    // Resetuj formularz po wysłaniu
    document.getElementById('contact-form').reset();
});
