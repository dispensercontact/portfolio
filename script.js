// Add smooth scrolling to all links
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

// Add animations on scroll
const sections = document.querySelectorAll('.section');
const options = {
    threshold: 0.3
};

const observer = new IntersectionObserver((entries, observer) => {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            entry.target.classList.add('visible');
            observer.unobserve(entry.target);
        }
    });
}, options);

sections.forEach(section => {
    observer.observe(section);
});

// Add animation class
document.querySelectorAll('.section').forEach(section => {
    section.classList.add('hidden');
});

// Inicjalizacja AOS
AOS.init({
    duration: 800, // Czas trwania animacji w milisekundach
    easing: 'ease-in-out', // Typ łagodzenia animacji
    once: true // Animacja tylko raz
});

// Animacje GSAP
document.addEventListener('DOMContentLoaded', () => {
    gsap.from('header', { duration: 0.8, y: '-100%', ease: 'bounce' });
    gsap.from('nav ul li', { duration: 0.6, opacity: 0, stagger: 0.1, delay: 0.8 });
    gsap.from('.container h1', { duration: 0.6, opacity: 0, y: 30, ease: 'power2.out', delay: 1.2 });
    gsap.from('.container p', { duration: 0.6, opacity: 0, y: 30, ease: 'power2.out', delay: 1.4 });
});
